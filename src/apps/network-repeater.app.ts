import { TPropertyObject } from 'tscommons-es-core';

import { CommonsStrictExpressServer } from 'nodecommons-es-express';
import { ICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsSocketIoApp } from 'nodecommons-es-app-socket-io';
import { commonsOutputDebug, commonsOutputDie, commonsOutputDoing, commonsOutputSuccess } from 'nodecommons-es-cli';

import { InjectionApi } from '../apis/injection.api';

import { RepeaterSocketIoServer } from '../servers/repeater-socket-io.server';

import { isTRepeaterConfig } from '../types/trepeater-config';
import { isTRestConfig } from '../types/trest-config';
import { isTPeersConfig } from '../types/tpeers-config';

export class NetworkRepeaterApp extends CommonsSocketIoApp<RepeaterSocketIoServer> {
	private selfUrl: string|undefined;
	private peerUrls: string[];
	private seenSize: number;
	private ignoreKeepAlive: boolean;
	
	constructor() {
		super('network-repeater');
		
		const repeaterConfig: TPropertyObject = this.getConfigArea('repeater', true);
		if (!isTRepeaterConfig(repeaterConfig)) commonsOutputDie('Repeater config is not valid');
		
		this.seenSize = repeaterConfig.seenSize || 1000;
		this.ignoreKeepAlive = repeaterConfig.ignoreKeepAlive || false;
		
		const peersConfig: TPropertyObject = this.getConfigArea('peers');
		if (!isTPeersConfig(peersConfig)) commonsOutputDie('Peers config is not valid');
		
		this.peerUrls = peersConfig.urls;
		this.selfUrl = peersConfig.selfUrl;

		commonsOutputDebug(`Self url is: ${this.selfUrl}`);
		
		const restConfig: TPropertyObject = this.getConfigArea('rest');
		if (!isTRestConfig(restConfig)) commonsOutputDie('Rest config is not valid');
		
		if (restConfig.enabled) {
			if (!restConfig.key) commonsOutputDie('No REST API key specified');

			this.install(
					(restServer: CommonsRestServer, p: string): void => {
						commonsOutputDoing('Creating REST injection server');
						
						// tslint:disable-next-line:no-unused-expression
						new InjectionApi(
								restServer,
								restConfig.key!,
								(): RepeaterSocketIoServer|undefined => this.getSocketIoServer(),
								p
						);
						
						commonsOutputSuccess();
					}
			);
		}
	}
	
	protected buildSocketIoServer(
			server: CommonsStrictExpressServer,
			config: ICommonsExpressConfig
	): RepeaterSocketIoServer {
		return new RepeaterSocketIoServer(
				server,
				config,
				this.selfUrl,	// |undefined is ok
				this.seenSize,
				this.ignoreKeepAlive,
				this.getArgs().hasAttribute('daemon')
		);
	}

	protected listening(): void {
		const repeaterSocketIoServer: RepeaterSocketIoServer|undefined = this.getSocketIoServer();
		if (!repeaterSocketIoServer) return;

		for (const peer of this.peerUrls) {
			repeaterSocketIoServer.addPeer(peer);
		}
	}
}
