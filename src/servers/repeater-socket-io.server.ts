import { Socket } from 'socket.io';

import {
		commonsBase62HasPropertyIdArray,
		commonsTypeIsEncodedObject,
		TPropertyObject, TEncodedObject,
		commonsBase62GenerateRandomId,
		commonsTypeDecodePropertyObject,
		commonsTypeEncodePropertyObject
} from 'tscommons-es-core';
import {
		ICommonsNetworkPacket,
		isICommonsNetworkPacket,
		ETransmissionMethod,
		TDeviceId,
		TChannel,
		compressPacket,
		decompressPacket,
		ICommonsNetworkHandshake,
		isICommonsNetworkHandshake,
		isTCommonsNetworkPacketCompressed
} from 'tscommons-es-network';

import { CommonsStrictExpressServer, ICommonsExpressConfig } from 'nodecommons-es-express';
import { CommonsAppSocketIoServer } from 'nodecommons-es-app-socket-io';
import {
		commonsOutputAlert,
		commonsOutputCompleted,
		commonsOutputDebug,
		commonsOutputDoing,
		commonsOutputError,
		commonsOutputInfo,
		commonsOutputPoll,
		commonsOutputResult
} from 'nodecommons-es-cli';

import { PeerSocketIoClientService } from '../services/peer-socket-io-client.service';

import { INetworkPeer, isINetworkPeer } from '../interfaces/inetwork-peer';

import { TIdMap } from '../types/tidmap';
import { TPeer } from '../types/tpeer';
import { TStatistics } from '../types/tstatistics';
import { TTransaction } from '../types/ttransaction';
import { TReplayRequest, isTReplayRequest } from '../types/treplay-request';

export class RepeaterSocketIoServer extends CommonsAppSocketIoServer {
	private peerId: TDeviceId = commonsBase62GenerateRandomId();
	
	private map: Map<string, TIdMap>;
	private clientPool: Map<TChannel, Map<TDeviceId, Map<string, Socket>>>;
	private incomingPeers: Map<string, TPeer>;
	private outgoingPeers: Map<string, PeerSocketIoClientService>;

	private seen: string[] = [];
	private archive: Map<string, TTransaction> = new Map<string, TTransaction>();

	private statistics: TStatistics = {
			received: 0,
			shared: 0,
			transmitted: 0
	};
	
	constructor(
			expressServer: CommonsStrictExpressServer,
			expressConfig: ICommonsExpressConfig,
			private selfUrl: string|undefined,
			private seenSize: number,
			private ignoreKeepAlive: boolean,
			private isDaemon: boolean
	) {
		super(expressServer, expressConfig);
		
		this.map = new Map<string, TIdMap>();
		this.clientPool = new Map<TChannel, Map<TDeviceId, Map<string, Socket>>>();
		this.incomingPeers = new Map<string, TPeer>();
		this.outgoingPeers = new Map<string, PeerSocketIoClientService>();
		
		this.setup();
		
		setInterval(
				(): void => {
					this.monitor();
				},
				1000
		);
	}
	
	private monitor(): void {
		let total: number = 0;
		for (const channel of Array.from(this.clientPool.keys())) {
			total += this.clientPool.get(channel)!.size;
		}
		
		commonsOutputPoll(`${total} clients; ${this.incomingPeers.size} incoming peers; ${this.outgoingPeers.size} outgoing peers; ${this.seen.length} seen; ${this.statistics.received} received; ${this.statistics.shared} shared; ${this.statistics.transmitted} transmitted\r`);
	}
	
	public addPeer(url: string) {
		if (this.outgoingPeers.has(url)) return;

		if (!this.selfUrl) throw new Error('No selfUrl has been defined. This should not be possible');

		if (this.isDaemon) {
			commonsOutputDebug(`Attempting to add peer ${url}`);
		} else {
			commonsOutputInfo(`Attempting to add peer ${url}`);
		}
		
		const outgoing: PeerSocketIoClientService = new PeerSocketIoClientService(
				url,
				this.peerId,
				this.selfUrl,
				(packet: ICommonsNetworkPacket, compress: boolean): void => {
					this.statistics.shared++;
					this.receive(packet, compress);
				},
				(): void => {	// disconnected
					commonsOutputDebug(`Disconnected from peer ${url} (this can include refusal to accept new connection due to existing one)`);

					// don't remove from the outgoingPeers map, as reconnection may reestablish the connecction
					// i.e. disconnect is a state, not a termination
				},
				this.isDaemon
		);
		
		outgoing.addConnectCallback((): void => {
			this.outgoingPeers.set(url, outgoing);	// it's ok to set this multiple times for connect/disconnect cycles
			
			commonsOutputCompleted(`Connected to peer ${url}`);
		});
		
		outgoing.establishConnection();
	}
	
	protected override onConnect(id: string, socket: Socket): boolean {
		if (isINetworkPeer(socket.handshake.query)) {
			const peer: INetworkPeer = socket.handshake.query as INetworkPeer;
			commonsOutputAlert(`Peer connection from ${peer.peerId} at ${peer.url}`);
			
			const existing: Socket[] = [];
			for (const key of this.incomingPeers.keys()) {
				const p: TPeer = this.incomingPeers.get(key)!;
				
				if (p.peer.peerId === peer.peerId) existing.push(p.socket);
			}
			
			if (existing.length > 0) {
				if (this.isDaemon) {
					commonsOutputDebug(`There are ${existing.length} incoming peer connections for this peer already. Skipping`);
				} else {
					commonsOutputInfo(`There are ${existing.length} incoming peer connections for this peer already. Skipping`);
				}
				return false;
			}
			
			this.incomingPeers.set(id, {
					peer: peer,
					socket: socket
			});

			// attempt a reverse connection if not already connected
			this.addPeer(peer.url);
			
			return true;
		}
		
		if (!isICommonsNetworkHandshake(socket.handshake.query)) {
			commonsOutputError('Client attempted to connect with invalid handshake. Disconnecting.', JSON.stringify(socket.handshake.query));
			return false;
		}

		const handshake: ICommonsNetworkHandshake = socket.handshake.query as ICommonsNetworkHandshake;
		commonsOutputCompleted(`Client ${handshake.deviceId} connected on channel ${handshake.channel}`);
		
		this.map.set(id, { channel: handshake.channel, deviceId: handshake.deviceId });
		
		if (!this.clientPool.has(handshake.channel)) this.clientPool.set(handshake.channel, new Map<TDeviceId, Map<string, Socket>>());
		if (!this.clientPool.get(handshake.channel)!.has(handshake.deviceId)) this.clientPool.get(handshake.channel)!.set(handshake.deviceId, new Map<string, Socket>());
		this.clientPool.get(handshake.channel)!.get(handshake.deviceId)!.set(id, socket);
		
		return true;
	}

	protected override onDisconnect(id: string): void {
		if (this.incomingPeers.has(id)) {
			const peer: TPeer = this.incomingPeers.get(id)!;
			this.incomingPeers.delete(id);

			commonsOutputAlert(`Peer ${peer.peer.peerId} at ${peer.peer.url} disconnected`);
			
			// allow this to continue in case the very unlikely chance that a peer is also a client.
		}
		
		if (!this.map.has(id)) return;
		
		const idMap: TIdMap = this.map.get(id)!;
		this.map.delete(id);
		
		commonsOutputAlert(`Client ${idMap.deviceId} on channel ${idMap.channel} disconnected`);

		if (!this.clientPool.has(idMap.channel)) return;	// unlikely
		if (!this.clientPool.get(idMap.channel)!.has(idMap.deviceId)) return;	// unlikely
		if (!this.clientPool.get(idMap.channel)!.get(idMap.deviceId)!.has(id)) return;	// unlikely
		
		this.clientPool.get(idMap.channel)!.get(idMap.deviceId)!.delete(id);
		
		if (this.clientPool.get(idMap.channel)!.get(idMap.deviceId)!.size === 0) this.clientPool.get(idMap.channel)!.delete(idMap.deviceId);
		if (this.clientPool.get(idMap.channel)!.size === 0) this.clientPool.delete(idMap.channel);
	}

	private setup(): void {
		super.on(
				'tx',
				(data: any): any => {
					if (!commonsTypeIsEncodedObject(data)) {
						commonsOutputError('Invalid packet received. Ignoring. (Not TEncodedObject)', JSON.stringify(data));
						return;
					}
					
					const object: TPropertyObject = commonsTypeDecodePropertyObject(data);
					
					if (isICommonsNetworkPacket(object)) {
						commonsOutputDebug('Packet is not compressed');
						const transmitted: boolean = this.receive(object, false);
	
						return transmitted;
					} else if (isTCommonsNetworkPacketCompressed(object)) {
						commonsOutputDebug('Packet is compressed');
						const packet: ICommonsNetworkPacket = decompressPacket(object);
						const transmitted: boolean = this.receive(packet, true);
	
						return transmitted;
					}
					
					commonsOutputError('Invalid packet received. Ignoring. (Neither compressed or uncompressed)', JSON.stringify(data));
					return;
				}
		);

		super.on(
				'replay',
				(data: any): string|undefined => {
					if (!isTReplayRequest(data)) {
						commonsOutputError('Invalid replay request received. Ignoring.', JSON.stringify(data));
						return undefined;
					}
					
					const request: TReplayRequest = data;

					commonsOutputDoing(`Request for replay since ${request.since} from ${request.deviceId}`);

					const transactions: TTransaction[] = this.since(request.since, request.channel, request.deviceId);
					
					commonsOutputResult(`${transactions.length} packets`);
					for (const transaction of transactions) {
						const packet: ICommonsNetworkPacket = transaction.packet;
						packet.replay = true;	// NB this changes the original archive entry, but doesn't really matter
						
						this.clientPool.get(request.channel)!.get(request.deviceId)!.forEach((socket: Socket): void => {
							// don't await for this, so transmission is in a parallel
							void this.transmit(socket, packet, transaction.compress);
						});
					}

					if (transactions.length === 0) return undefined;
					return transactions[transactions.length - 1].packet.id;
				}
		);
	}
	
	public push(packet: ICommonsNetworkPacket): void {
		if (this.isDaemon) {
			commonsOutputDebug('Pushing packet', packet.id);
		} else {
			commonsOutputInfo('Pushing packet', packet.id);
		}
		this.receive(packet, false);
	}
	
	private receive(packet: ICommonsNetworkPacket, compress: boolean = false): boolean {
		if (this.ignoreKeepAlive && packet.ns === 'network' && packet.command === 'keep-alive') {
			// skip re-broadcasting keep-alive packets
			
			commonsOutputDebug(`Ignoring keep-alive packet from ${packet.source}`);
			
			return true;
		}
		
		if (this.isDaemon) {
			commonsOutputDebug(`Received packet ${packet.id} from ${packet.source} on channel ${packet.channel}: ${packet.ns}/${packet.command}`);
		} else {
			commonsOutputInfo(`Received packet ${packet.id} from ${packet.source} on channel ${packet.channel}: ${packet.ns}/${packet.command}`);
		}
		
		this.statistics.received++;
		
		if (packet.expiry && packet.expiry.getTime() < new Date().getTime()) {
			if (this.isDaemon) {
				commonsOutputDebug('Packet expired. Ignoring');
			} else {
				commonsOutputAlert('Packet expired. Ignoring');
			}
			return false;
		}

		if (this.seen.includes(packet.id)) {
			if (this.isDaemon) {
				commonsOutputDebug('Packet already seen. Ignoring');
			} else {
				commonsOutputInfo('Packet already seen. Ignoring');
			}
			return false;
		}

		const transaction: TTransaction = {
				packet: packet,
				compress: compress
		};
		
		this.archive.set(packet.id, transaction);
		this.seen.push(packet.id);
		
		while (this.seen.length >= this.seenSize) {
			const id: string|undefined = this.seen.shift();
			if (id === undefined) break;
			this.archive.delete(id);
		}

		switch (packet.method) {
			case ETransmissionMethod.BROADCAST:
				this.broadcastPacket(packet, compress);
				break;
			case ETransmissionMethod.DIRECT:
				this.directPacket(packet, compress);
				break;
			case ETransmissionMethod.MULTICAST:
				this.multicastPacket(packet, compress);
				break;
		}
		
		const encoded: TEncodedObject = commonsTypeEncodePropertyObject(packet, true);
		for (const peer of this.incomingPeers.values()) {
			if (this.isDaemon) {
				commonsOutputDebug('Sharing packet to peer at', peer.peer.url);
			} else {
				commonsOutputInfo('Sharing packet to peer at', peer.peer.url);
			}
			
			// don't await
			void super.direct(peer.socket, 'share', {
					compress: compress,
					encoded: encoded
			});
		}
		
		return true;
	}
	
	private since(lastSeen: string, channel: TChannel, deviceId: TDeviceId): TTransaction[] {
		if (!this.seen.includes(lastSeen)) return [];
		
		const index: number = this.seen.indexOf(lastSeen);
		if (index < 0 || index >= (this.seen.length - 1)) return [];
	
		const transactions: TTransaction[] = this.seen
				.slice(index + 1)
				.map((id: string): TTransaction|undefined => {
					if (!this.archive.has(id)) commonsOutputError(`MISSING IN ARCHIVE FOR ${id}`);
					return this.archive.get(id);
				})
				.filter((t: TTransaction|undefined): boolean => {
					if (t === undefined) return false;
					commonsOutputDebug(`Considering packet: ${JSON.stringify(t.packet)}`);

					return true;
				})
				.map((t: TTransaction|undefined): TTransaction => t!)
				.filter((t: TTransaction): boolean => t.packet.expiry === undefined || t.packet.expiry.getTime() > new Date().getTime())
				.filter((t: TTransaction): boolean => t.packet.channel === channel)
				.filter((t: TTransaction): boolean => {
					switch (t.packet.method) {
						case ETransmissionMethod.BROADCAST:
							return true;
						case ETransmissionMethod.DIRECT:
							return t.packet.destination === deviceId;
						case ETransmissionMethod.MULTICAST:
							if (!commonsBase62HasPropertyIdArray(t.packet, 'destinations')) return false;
							return t.packet.destinations!.includes(deviceId);
					}
				});

		commonsOutputDebug(`Number of transactions is ${transactions.length}`);
		return transactions;
	}
	
	private async transmit(socket: Socket, packet: ICommonsNetworkPacket, compress: boolean): Promise<boolean> {
		let encoded: TEncodedObject = {};
		
		if (compress) encoded = commonsTypeEncodePropertyObject(compressPacket(packet), true);
		else encoded = commonsTypeEncodePropertyObject(packet, true);
		
		try {
			await super.direct(socket, 'rx', encoded);
			this.statistics.transmitted++;
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	private broadcastPacket(packet: ICommonsNetworkPacket, compress: boolean): boolean {
		if (!this.clientPool.has(packet.channel)) {
			commonsOutputError('Attempt to broadcast to an empty channel. Ignoring');
			return false;
		}
		
		commonsOutputDebug('Broadcasting packet');
		this.clientPool.get(packet.channel)!.forEach((sockets: Map<string, Socket>): void => {
			sockets.forEach((socket: Socket): void => {
				// don't await for this, so transmission is in a parallel
				void this.transmit(socket, packet, compress);
			});
		});
		
		return true;
	}

	private directPacket(packet: ICommonsNetworkPacket, compress: boolean): boolean {
		if (!this.clientPool.has(packet.channel)) {
			commonsOutputError('Attempt to direct to an empty channel. Ignoring');
			return false;
		}
		if (packet.destination === undefined) {
			commonsOutputError('Attempt to direct to no destination. Ignoring');
			return false;
		}
		if (!this.clientPool.get(packet.channel)!.has(packet.destination)) {
			commonsOutputError('Attempt to direct to an unknown device. Ignoring');
			return false;
		}
		
		this.clientPool.get(packet.channel)!.get(packet.destination)!.forEach((socket: Socket): void => {
			// don't await for this, so transmission is in a parallel
			void this.transmit(socket, packet, compress);
		});
		
		return true;
	}
	
	private multicastPacket(packet: ICommonsNetworkPacket, compress: boolean): boolean {
		if (!packet.destinations || packet.destinations.length === 0) {
			commonsOutputError('Attempt to multidirect to no destinations. Ignoring');
			return false;
		}
		
		const destinations: string[] = packet.destinations.slice();	// copy

		if (!this.clientPool.has(packet.channel)) {
			commonsOutputError('Attempt to multidirect to an empty channel. Ignoring');
			return false;
		}

		for (const destination of destinations) {
			if (!this.clientPool.get(packet.channel)!.has(destination)) {
				commonsOutputError('Attempt to multidirect to an unknown device. Skipping.');
				continue;
			}
			
			packet.destinations = [ destination ];	// hide others

			this.clientPool.get(packet.channel)!.get(destination)!.forEach((socket: Socket): void => {
				// don't await for this, so transmission is in a parallel
				void this.transmit(socket, packet, compress);
			});
		}
		
		return true;
	}
}
