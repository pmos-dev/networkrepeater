import * as express from 'express';

import { TPropertyObject, commonsBase62GenerateRandomId, commonsTypeDecodePropertyObject, commonsTypeIsEncodedObject, TEncoded } from 'tscommons-es-core';
import {
		ICommonsNetworkPacket,
		isICommonsNetworkPacket,
		ETransmissionMethod
} from 'tscommons-es-network';

import { CommonsRestServer, CommonsRestKeyApi, commonsRestApiBadRequest } from 'nodecommons-es-rest';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';

import { RepeaterSocketIoServer } from '../servers/repeater-socket-io.server';

type TInjectionUidResult = {
		uid: string;
};

export class InjectionApi extends CommonsRestKeyApi {
	constructor(
			restServer: CommonsRestServer,
			key: string,
			private getRepeaterSocketIoServer: () => RepeaterSocketIoServer|undefined,
			path?: string
	) {
		super(restServer, key);

		if (path === undefined) path = '/';
		else path = `${path}${path.endsWith('/') ? '' : '/'}`;
		
		super.postHandler(
				`${path}broadcast/:channel[base62]/:ns[idname]/:command[idname]`,
				// eslint-disable-next-line @typescript-eslint/require-await
				async (req: ICommonsRequestWithStrictParams, _: express.Response): Promise<TInjectionUidResult> => {
					if (!/^[-_a-z0-9]{1,64}$/i.test(req.strictParams.ns as string)) return commonsRestApiBadRequest('Invalid namespace supplied');
					if (!/^[-_a-z0-9]{1,64}$/i.test(req.strictParams.command as string)) return commonsRestApiBadRequest('Invalid command supplied');
				
					const packet: ICommonsNetworkPacket = {
							id: commonsBase62GenerateRandomId(),
							method: ETransmissionMethod.BROADCAST,
							channel: req.strictParams.channel as string,
							source: '00inject',
							timestamp: new Date(),
							ns: req.strictParams.ns as string,
							command: req.strictParams.command as string,
							data: req.body as TEncoded
					};
					
					if (!isICommonsNetworkPacket(packet)) return commonsRestApiBadRequest('Resulting push packet was invalid');
					
					const repeaterSocketIoServer: RepeaterSocketIoServer|undefined = this.getRepeaterSocketIoServer();
					if (repeaterSocketIoServer) repeaterSocketIoServer.push(packet);
					
					return {
							uid: packet.id
					};
				}
		);

		super.postHandler(
				`${path}direct/:channel[base62]/:destination[base62]/:ns[idname]/:command[idname]`,
				// eslint-disable-next-line @typescript-eslint/require-await
				async (req: ICommonsRequestWithStrictParams, _: express.Response): Promise<TInjectionUidResult> => {
					if (!/^[-_a-z0-9]{1,64}$/i.test(req.strictParams.ns as string)) return commonsRestApiBadRequest('Invalid namespace supplied');
					if (!/^[-_a-z0-9]{1,64}$/i.test(req.strictParams.command as string)) return commonsRestApiBadRequest('Invalid command supplied');

					const packet: ICommonsNetworkPacket = {
							id: commonsBase62GenerateRandomId(),
							method: ETransmissionMethod.DIRECT,
							channel: req.strictParams.channel as string,
							destination: req.strictParams.destination as string,
							source: '00inject',
							timestamp: new Date(),
							ns: req.strictParams.ns as string,
							command: req.strictParams.command as string,
							data: req.body as TEncoded
					};
					
					if (!isICommonsNetworkPacket(packet)) return commonsRestApiBadRequest('Resulting push packet was invalid');
					
					const repeaterSocketIoServer: RepeaterSocketIoServer|undefined = this.getRepeaterSocketIoServer();
					if (repeaterSocketIoServer) repeaterSocketIoServer.push(packet);

					return {
							uid: packet.id
					};
				}
		);

		super.postHandler(
				`${path}raw`,
				// eslint-disable-next-line @typescript-eslint/require-await
				async (req: express.Request, _: express.Response): Promise<TInjectionUidResult> => {
					if (!commonsTypeIsEncodedObject(req.body)) return commonsRestApiBadRequest('Invalid raw push packet at TEncodedObject stage');
					const packet: TPropertyObject = commonsTypeDecodePropertyObject(req.body);
					if (!isICommonsNetworkPacket(packet)) return commonsRestApiBadRequest('Invalid raw push packet at ICommonsNetworkPacket stage');
					
					const repeaterSocketIoServer: RepeaterSocketIoServer|undefined = this.getRepeaterSocketIoServer();
					if (repeaterSocketIoServer) repeaterSocketIoServer.push(packet);

					return {
							uid: packet.id
					};
				}
		);

		super.getHandler(
				`${path}test`,
				// eslint-disable-next-line @typescript-eslint/require-await
				async (_req: express.Request, _res: express.Response): Promise<boolean> => true
		);
	}
}
