export type TStatistics = {
		received: number;
		shared: number;
		transmitted: number;
};
