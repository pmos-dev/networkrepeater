import { commonsTypeHasPropertyString, commonsTypeHasPropertyStringArray } from 'tscommons-es-core';

export type TPeersConfig = {
		urls: string[];
		selfUrl: string;
};

export function isTPeersConfig(test: any): test is TPeersConfig {
	if (!commonsTypeHasPropertyStringArray(test, 'urls')) return false;
	if (!commonsTypeHasPropertyString(test, 'selfUrl')) return false;

	return true;
}
