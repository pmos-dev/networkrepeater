import { ICommonsNetworkPacket } from 'tscommons-es-network';

export type TTransaction = {
		packet: ICommonsNetworkPacket;
		compress: boolean;
};
