import { commonsTypeHasPropertyBooleanOrUndefined, commonsTypeHasPropertyNumberOrUndefined } from 'tscommons-es-core';

export type TRepeaterConfig = {
		seenSize?: number;
		ignoreKeepAlive?: boolean;
};

export function isTRepeaterConfig(test: any): test is TRepeaterConfig {
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'seenSize')) return false;
	if (!commonsTypeHasPropertyBooleanOrUndefined(test, 'ignoreKeepAlive')) return false;

	return true;
}
