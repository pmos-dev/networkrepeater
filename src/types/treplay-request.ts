import { commonsBase62HasPropertyId } from 'tscommons-es-core';
import { TChannel, TDeviceId } from 'tscommons-es-network';

export type TReplayRequest = {
		since: string;
		channel: TChannel;
		deviceId: TDeviceId;
};

export function isTReplayRequest(test: any): test is TReplayRequest {
	if (!commonsBase62HasPropertyId(test, 'since')) return false;
	if (!commonsBase62HasPropertyId(test, 'channel')) return false;
	if (!commonsBase62HasPropertyId(test, 'deviceId')) return false;

	return true;
}
