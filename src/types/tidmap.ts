import { TDeviceId } from 'tscommons-es-network';

export type TIdMap = {
		channel: string;
		deviceId: TDeviceId;
};
