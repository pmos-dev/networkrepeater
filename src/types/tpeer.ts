import { Socket } from 'socket.io';

import { INetworkPeer } from '../interfaces/inetwork-peer';

export type TPeer = {
		peer: INetworkPeer;
		socket: Socket;
};
