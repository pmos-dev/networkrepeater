import { commonsTypeHasPropertyBoolean, commonsTypeHasPropertyStringOrUndefined } from 'tscommons-es-core';

export type TRestConfig = {
		enabled: boolean;
		key?: string;
};

export function isTRestConfig(test: any): test is TRestConfig {
	if (!commonsTypeHasPropertyBoolean(test, 'enabled')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'key')) return false;

	return true;
}
