#!/usr/bin/env node

import { commonsOutputDebug, commonsOutputSetDaemon, commonsOutputSetDebugging } from 'nodecommons-es-cli';
import { CommonsArgs } from 'nodecommons-es-cli';

import { NetworkRepeaterApp } from './apps/network-repeater.app';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);
if (args.hasAttribute('daemon')) commonsOutputSetDaemon(true);

const app: NetworkRepeaterApp = new NetworkRepeaterApp();

app.autoSystemd(
		'/usr/bin/network-repeater --daemon',
		'Network Repeater'
);

void (async (): Promise<void> => {
	commonsOutputDebug('Starting application: NetworkRepeater');
	await app.start();
	commonsOutputDebug('Application completed');
	
	setTimeout((): void => {
		//log() // logs out active handles that are keeping node running
		process.exit(0);
	}, 100);
})();
