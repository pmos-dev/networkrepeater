import {
		commonsTypeDecodePropertyObject,
		commonsTypeHasPropertyBoolean,
		commonsTypeHasPropertyObject,
		commonsTypeIsBoolean,
		commonsTypeIsEncodedObject,
		TPropertyObject
} from 'tscommons-es-core';
import { ICommonsNetworkPacket, isICommonsNetworkPacket } from 'tscommons-es-network';

import { commonsSocketIoBuildOptions, commonsSocketIoBuildUrl, CommonsSocketIoClientService } from 'nodecommons-es-socket-io';
import { commonsOutputDebug, commonsOutputError } from 'nodecommons-es-cli';

import { INetworkPeer } from '../interfaces/inetwork-peer';

export class PeerSocketIoClientService extends CommonsSocketIoClientService {
	constructor(
			url: string,
			private ownPeerId: string,
			private ownUrl: string,
			private received: (_packet: ICommonsNetworkPacket, _compress: boolean) => void,
			disconnected: () => void,
			isDaemon: boolean = false
	) {
		super(
				commonsSocketIoBuildUrl(url),
				true,
				commonsSocketIoBuildOptions(url)
		);
		
		super.addDisconnectCallback(disconnected);
		
		super.addErrorCallback(
				(error: Error): void => {
					if (isDaemon) {
						commonsOutputDebug(`Socket.io-client error: ${error.message}`);
					} else {
						commonsOutputError(`Socket.io-client error: ${error.message}`);
					}
				}
		);
	}
	
	public establishConnection(): void {
		const peer: INetworkPeer = {
				peerId: this.ownPeerId,
				url: this.ownUrl
		};
		
		super.connect(peer as Required<INetworkPeer>);
	}
	
	protected setupOns(): void {
		super.on('share', (data: any): void => {
			commonsOutputDebug('Received shared packet');
			
			if (!commonsTypeIsEncodedObject(data)) return;
			if (!commonsTypeHasPropertyObject(data, 'encoded')) return;
			if (!commonsTypeIsEncodedObject(data.encoded)) return;
			
			const object: TPropertyObject = commonsTypeDecodePropertyObject(data.encoded);
			
			if (!isICommonsNetworkPacket(object)) return;
			
			if (!commonsTypeHasPropertyBoolean(data, 'compress')) return;
			if (!commonsTypeIsBoolean(data.compress)) return;	// just to satisfy the strict type guard

			this.received(object, data.compress);
		});
	}
}
