import { commonsBase62HasPropertyId, commonsTypeHasPropertyString, commonsTypeIsObject } from 'tscommons-es-core';
import { TDeviceId } from 'tscommons-es-network';

export interface INetworkPeer {
		peerId: TDeviceId;
		url: string;
}

export function isINetworkPeer(test: any): test is INetworkPeer {
	if (!commonsTypeIsObject(test)) return false;

	if (!commonsBase62HasPropertyId(test, 'peerId')) return false;
	if (!commonsTypeHasPropertyString(test, 'url')) return false;

	return true;
}
